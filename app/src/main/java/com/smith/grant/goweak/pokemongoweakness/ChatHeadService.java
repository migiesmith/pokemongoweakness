package com.smith.grant.goweak.pokemongoweakness;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatHeadService extends Service implements OnTouchListener {

    private WindowManager windowManager;
    private ImageView movableIcon;
    WindowManager.LayoutParams movableIconParams;

    private ImageView closeService;
    WindowManager.LayoutParams closeServiceParams;

    // Dragging variables
    private int initialX;
    private int initialY;
    private float initialTouchX;
    private float initialTouchY;
    private static final int MAX_CLICK_DURATION = 200;
    private long startClickTime;

    private boolean enteredClosingZone = false;

    Pokemon[] pokemon = new Pokemon[151];
    Map<String, TypeEffectiveness> typeEffectiveness = new HashMap<String, TypeEffectiveness>();


    @Override
    public void onCreate() {
        super.onCreate();

        // Start ads
        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.banner_ad_unit_id));

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        movableIcon = new ImageView(this);
        movableIcon.setClickable(true);
        movableIcon.setImageResource(R.drawable.android_head);

        movableIconParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        movableIconParams.gravity = Gravity.TOP | Gravity.LEFT;
        movableIconParams.x = 0;
        movableIconParams.y = 100;
        movableIconParams.width = 128;
        movableIconParams.height = 128;


        closeService = new ImageView(this);
        closeService.setImageResource(R.drawable.pokeball);
        closeService.setVisibility(View.INVISIBLE);

        closeServiceParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        closeServiceParams.gravity = Gravity.TOP | Gravity.LEFT;
        Point screenDimensions = new Point();
        windowManager.getDefaultDisplay().getSize(screenDimensions);

        closeServiceParams.width = 64;
        closeServiceParams.height = 64;
        closeServiceParams.x = screenDimensions.x/2 - 32;
        closeServiceParams.y = screenDimensions.y - 80;

        loadPokemon();
        loadTypeEffectiveness();

        // Set the listener and add the movable icon
        movableIcon.setOnTouchListener(this);
        windowManager.addView(movableIcon, movableIconParams);

    }

    // Load in all of the required pokemon information
    private void loadPokemon(){
        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.pokemoninfo), "Unicode"));
            String line = is.readLine();
            while(line != null){
                String[] contents = line.split("\t");
                if(contents.length >= 3) {
                    int pokeNo = Integer.valueOf(contents[0]);
                    String name = contents[1];
                    String type = contents[2].toUpperCase();
                    if (contents.length == 4)
                        type += "-"+contents[3].toUpperCase();

                    pokemon[pokeNo-1] = new Pokemon(this, pokeNo, name, type);
                }
                line = is.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadTypeEffectiveness(){
        try {
            BufferedReader is = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.typeeffectiveness), "Unicode"));
            String line = is.readLine();
            while(line != null){
                String[] contents = line.split("\t");
                if(!line.startsWith("ATTACK") && !line.startsWith("DEFENSE")) {
                    TypeEffectiveness effectiveness = new TypeEffectiveness();
                    for(int i = 1; i < contents.length; i++){
                        switch(i){
                            case 1:
                                effectiveness.types.put("NORMAL", Double.valueOf(contents[i]));
                                break;
                            case 2:
                                effectiveness.types.put("FIRE", Double.valueOf(contents[i]));
                                break;
                            case 3:
                                effectiveness.types.put("WATER", Double.valueOf(contents[i]));
                                break;
                            case 4:
                                effectiveness.types.put("ELECTRIC", Double.valueOf(contents[i]));
                                break;
                            case 5:
                                effectiveness.types.put("GRASS", Double.valueOf(contents[i]));
                                break;
                            case 6:
                                effectiveness.types.put("ICE", Double.valueOf(contents[i]));
                                break;
                            case 7:
                                effectiveness.types.put("FIGHTING", Double.valueOf(contents[i]));
                                break;
                            case 8:
                                effectiveness.types.put("POISON", Double.valueOf(contents[i]));
                                break;
                            case 9:
                                effectiveness.types.put("GROUND", Double.valueOf(contents[i]));
                                break;
                            case 10:
                                effectiveness.types.put("FLYING", Double.valueOf(contents[i]));
                                break;
                            case 11:
                                effectiveness.types.put("PSYCHIC", Double.valueOf(contents[i]));
                                break;
                            case 12:
                                effectiveness.types.put("BUG", Double.valueOf(contents[i]));
                                break;
                            case 13:
                                effectiveness.types.put("ROCK", Double.valueOf(contents[i]));
                                break;
                            case 14:
                                effectiveness.types.put("GHOST", Double.valueOf(contents[i]));
                                break;
                            case 15:
                                effectiveness.types.put("DRAGON", Double.valueOf(contents[i]));
                                break;
                            case 16:
                                effectiveness.types.put("DARK", Double.valueOf(contents[i]));
                                break;
                            case 17:
                                effectiveness.types.put("STEEL", Double.valueOf(contents[i]));
                                break;
                            case 18:
                                effectiveness.types.put("FAIRY", Double.valueOf(contents[i]));
                                break;
                        }
                    }
                    typeEffectiveness.put(contents[0], effectiveness);
                }
                line = is.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clickedOn(){
        movableIcon.setVisibility(View.INVISIBLE);

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        final View mainView = View.inflate(this, R.layout.main_view, null);


        WindowManager.LayoutParams mainViewParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        GridView grid = (GridView) mainView.findViewById(R.id.gridView);


        MainViewHandling adapter = new MainViewHandling(this);
        List<Pokemon> pokeList = new ArrayList<Pokemon>();
        for(Pokemon p : pokemon){
            pokeList.add(p);
        }
        Collections.sort(pokeList);
        adapter.pokemon = pokeList;
        adapter.typeEffectiveness = typeEffectiveness;
        adapter.mainView = true;
        grid.setAdapter(adapter);

        View.OnClickListener closeView = new OnClickListener() {
            @Override
            public void onClick(View view) {
                movableIcon.setVisibility(View.VISIBLE);
                windowManager.removeView(mainView);
            }
        };

        mainView.findViewById(R.id.closeButton).setOnClickListener(closeView);
        mainView.findViewById(R.id.pokelistBackground).setOnClickListener(closeView);

        windowManager.addView(mainView, mainViewParams);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (movableIcon != null)
            windowManager.removeView(movableIcon);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = movableIconParams.x;
                initialY = movableIconParams.y;
                initialTouchX = event.getRawX();
                initialTouchY = event.getRawY();
                startClickTime = Calendar.getInstance().getTimeInMillis();
                movableIcon.animate().translationX(0);

                windowManager.addView(closeService, closeServiceParams);
                return true;

            case MotionEvent.ACTION_UP:
                long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                if(clickDuration < MAX_CLICK_DURATION) {
                    clickedOn();
                }

                windowManager.removeView(closeService);
                closeService.setVisibility(View.INVISIBLE);

                if(Math.abs(closeServiceParams.x - movableIconParams.x) <= 80 && Math.abs(closeServiceParams.y - movableIconParams.y) <= 80){
                    Toast.makeText(this, "Closing " + getResources().getString(R.string.app_name) +".", Toast.LENGTH_SHORT).show();
                    stopService(new Intent(this, ChatHeadService.class));
                }

                if(movableIconParams.x <= 0) {
                    movableIcon.animate().translationX(-movableIconParams.width/2);
                }else{
                    Display display = ((WindowManager)getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    if(movableIconParams.x + movableIconParams.width >= size.x){
                        movableIcon.animate().translationX(movableIconParams.width/2);
                    }
                }

                windowManager.updateViewLayout(movableIcon, movableIconParams);

                return true;

            case MotionEvent.ACTION_MOVE:
                movableIconParams.x = initialX
                        + (int) (event.getRawX() - initialTouchX);
                movableIconParams.y = initialY
                        + (int) (event.getRawY() - initialTouchY);
                windowManager.updateViewLayout(movableIcon, movableIconParams);
                closeService.setVisibility(View.VISIBLE);

                if(Math.abs(closeServiceParams.x - movableIconParams.x) <= 80 && Math.abs(closeServiceParams.y - movableIconParams.y) <= 80){
                    if(!enteredClosingZone) {
                        enteredClosingZone = true;
                        ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(50);
                        movableIcon.animate().scaleX(0.5f);
                        movableIcon.animate().scaleY(0.5f);
                        closeService.animate().rotation(closeService.getRotation()+360.0f);

                    }
                }else{
                    if(enteredClosingZone) {
                        movableIcon.animate().scaleX(1.0f);
                        movableIcon.animate().scaleY(1.0f);
                        closeService.animate().rotation(closeService.getRotation()-360.0f);
                    }
                    enteredClosingZone = false;
                }

                return true;
        }
        return false;
    }

}