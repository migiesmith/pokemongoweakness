package com.smith.grant.goweak.pokemongoweakness;

import android.content.Context;
import android.util.Log;

/**
 * Created by Grant on 18/07/2016.
 */
public class Pokemon implements Comparable<Pokemon>{

    int id;
    int pokeNo;
    String name;
    String type;

    Pokemon(Context context, int pokeNo, String name, String type){
        this.pokeNo = pokeNo;
        this.name = name;
        this.type = type;

        id = context.getResources().getIdentifier("pk"+pokeNo, "drawable", context.getPackageName());
    }

    @Override
    public int compareTo(Pokemon other) {
        return name.compareTo(other.name);
    }
}
