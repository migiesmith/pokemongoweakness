package com.smith.grant.goweak.pokemongoweakness;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainViewHandling extends BaseAdapter {

    boolean mainView = false;

    List<Pokemon> pokemon;
    List<String> weakness;
    Map<String, TypeEffectiveness> typeEffectiveness;


    private Context context;

    List<View> views = new ArrayList<View>();

    public MainViewHandling(Context c) {
        context = c;
    }

    //---returns the number of images---
    public int getCount() {
        return pokemon.size();
    }

    //---returns the ID of an item---
    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    //---returns an ImageView view---
    public View getView(final int position, final View convertView, ViewGroup parent) {
        ImageView imageView;
        if (position >= views.size() || views.get(position) == null) {
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5, 5, 5, 5);
        } else {
            return views.get(position);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bmpSource = BitmapFactory.decodeResource(context.getResources(), pokemon.get(position).id, options);
        Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpSource, 100, 100, false);
        imageView.setImageBitmap(bmpScaled);

        RelativeLayout relLayout = new RelativeLayout(context);
        relLayout.setBackground(context.getResources().getDrawable(R.drawable.layout_pokemon));
        relLayout.addView(imageView);
        TextView txtView = new TextView(context);
        txtView.setTextColor(ContextCompat.getColor(context, R.color.textColour));
        txtView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        txtView.setText(pokemon.get(position).name);
        txtView.setGravity(Gravity.CENTER_HORIZONTAL);
        relLayout.addView(txtView);

        if(weakness != null){
            TextView weaknessText = new TextView(context);
            weaknessText.setTextColor(ContextCompat.getColor(context, R.color.textColour));


            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

            weaknessText.setLayoutParams(params);

            weaknessText.setText(weakness.get(position));
            weaknessText.setGravity(Gravity.CENTER_HORIZONTAL);
            relLayout.addView(weaknessText);
        }

        if(mainView) {
            relLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final View effectivenessView = View.inflate(context, R.layout.pokemon_layout, null);

                    Handler h = new Handler();
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            AdView mAdView = (AdView) effectivenessView.findViewById(R.id.adView);
                            AdRequest adRequest = new AdRequest.Builder()
                                    .addTestDevice("C816BE09052ABC607AD981474A467CB5")
                                    .build();
                            mAdView.loadAd(adRequest);
                        }
                    });


                    GridView grid2 = (GridView) effectivenessView.findViewById(R.id.weaknessGrid);

                    MainViewHandling adapter = new MainViewHandling(context);
                    List<Pokemon> effective = new ArrayList<Pokemon>();
                    List<String> weakness = new ArrayList<String>();
                    for (int i = 0; i < pokemon.size(); i++) {
                        Object[][] effect = checkEffectiveness(pokemon.get(i), pokemon.get(position));

                        if (effect == null)
                            continue;

                        boolean first = false, second = false;

                        if ((double) effect[0][1] > 1) {
                            first = true;
                        }
                        if (effect.length > 1 && (double) effect[1][1] > 1) {
                            second = true;
                        }

                        if (first || second) {
                            String weaknessStr = "";
                            if (first) {
                                weaknessStr += effect[0][0];
                            }
                            if (second) {
                                weaknessStr += (first ? " or " : "") + effect[1][0];
                            }
                            weakness.add(weaknessStr);

                            effective.add(pokemon.get(i));
                        }
                    }

                    adapter.pokemon = effective;
                    adapter.weakness = weakness;
                    adapter.typeEffectiveness = typeEffectiveness;
                    grid2.setAdapter(adapter);

                    TextView weaknessText = (TextView) effectivenessView.findViewById(R.id.weaknessText);
                    weaknessText.setText(effective.size() + " weaknesses:");


                    WindowManager.LayoutParams mainViewParams = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.TYPE_PHONE,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                            PixelFormat.TRANSLUCENT);


                    final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

                    View.OnClickListener closeView = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            windowManager.removeView(effectivenessView);
                        }
                    };

                    ImageView pokeImage = ((ImageView) effectivenessView.findViewById(R.id.pokeImage));
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = false;
                    options.inSampleSize = 1;
                    options.inScaled = false;
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                    Bitmap bmpSource = BitmapFactory.decodeResource(context.getResources(), pokemon.get(position).id, options);
                    Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpSource, pokeImage.getLayoutParams().width, pokeImage.getLayoutParams().height, false);
                    pokeImage.setImageBitmap(bmpScaled);

                    ((TextView) effectivenessView.findViewById(R.id.pokeName)).setText(pokemon.get(position).name);

                    effectivenessView.findViewById(R.id.effectCloseButton).setOnClickListener(closeView);
                    effectivenessView.findViewById(R.id.effectBackground).setOnClickListener(closeView);

                    windowManager.addView(effectivenessView, mainViewParams);

                }
            });
        }

        ((RelativeLayout.LayoutParams)imageView.getLayoutParams()).addRule(RelativeLayout.CENTER_IN_PARENT);

        views.add(relLayout);
        return relLayout;
    }

    private Object[][] checkEffectiveness(Pokemon attacker, Pokemon defender){
        if(attacker.pokeNo == defender.pokeNo){
            return null;
        }

        String[] attackerTypes = attacker.type.split("-");
        Object[][] effectiveness = new Object[attackerTypes.length][2];

        effectiveness[0][0] = attackerTypes[0];
        effectiveness[0][1] = typeEffectiveness.get(defender.type).types.get(attackerTypes[0]);
        if(attackerTypes.length > 1) {
            effectiveness[1][0] = attackerTypes[1];
            effectiveness[1][1] = typeEffectiveness.get(defender.type).types.get(attackerTypes[1]);
        }

        return effectiveness;
    }

}